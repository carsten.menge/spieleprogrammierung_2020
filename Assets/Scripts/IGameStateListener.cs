﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
  /// <summary>
  /// Interface for anything that needs Game State updates.
  /// </summary>
  public interface IGameStateListener
  {
    /// <summary>
    /// Called whenever the GameState updates
    /// </summary>
    /// <param name="gameState">The new GameState</param>
    void OnUpdateGameState(GameState gameState);
  }
}
