﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Simple PickupItem to give the player a weapon.
/// </summary>
public class PistolPickup : PickupItem, IWeapon
{
  public Projectile projectile;
  public float projectileSpeed;
  public Projectile Projectile => projectile;
  public float ProjectileSpeed => projectileSpeed;

  public override void OnPickup(PlayerInventory inv)
  {
    inv.PickItem(this);
    gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
    StartCoroutine(AnimatePickup());
  }
  /// <summary>
  /// Simple particle system to play on pickup.
  /// </summary>
  IEnumerator AnimatePickup()
  {
    var p = gameObject.GetComponent<ParticleSystem>();
    p.Play();
    yield return new WaitForSeconds(p.main.duration);
    Destroy(gameObject);
  }
}



