﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
  /// <summary>
  /// 
  /// </summary>
  public class GameState
  {
    /// <summary>
    /// Possible Game States.
    /// </summary>
    public enum States
    {
      /// <summary>
      /// The level is loading.
      /// </summary>
      LevelLoading,
      /// <summary>
      /// The level is loaded and waiting for the player to start.
      /// </summary>
      PreStart,
      /// <summary>
      /// The Game is running.
      /// </summary>
      GameRunning,
      /// <summary>
      /// --unused--
      /// </summary>
      GamePaused,
      /// <summary>
      /// The Level is won.
      /// </summary>
      GameWon,
      /// <summary>
      /// The level is lost.
      /// </summary>
      GameLost,
    }
    public States State;
    /// <summary>
    /// Stopwatch to time the level.
    /// </summary>
    public Stopwatch stopwatch;

    public GameState()
    {
      this.State = States.LevelLoading;
      this.stopwatch = new Stopwatch();
    }
    /// <summary>
    /// Reset the game to loading, so that entities can be reinitialized.
    /// </summary>
    public void ResetGameState()
    {
      this.State = States.LevelLoading;
      this.stopwatch.Reset();
    }
  }
}
