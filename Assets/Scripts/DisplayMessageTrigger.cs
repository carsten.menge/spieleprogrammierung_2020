﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Trigger for showing messages to the player. If repeating is true, the message gets queued every time the player enters the trigger.
/// </summary>
public class DisplayMessageTrigger : MonoBehaviour
{
  public string message = "";
  public long duration = 0;
  public bool repeating = false;
  public HUDController hud;

  private bool messagePlayed = false;
  private void OnTriggerEnter(Collider other)
  {
    if (other.GetComponent<PlayerController>() == null) return;
    if (repeating)
    {
      hud.DisplayMsgToPlayer(message, duration);
    } else if (!messagePlayed)
    {
      hud.DisplayMsgToPlayer(message, duration);
      messagePlayed = true; // could probably also just destroy the game object
    }  
  }

}
