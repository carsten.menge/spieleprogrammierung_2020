﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// A simple key to be used on a keyhole.
/// </summary>
public class GameKey : PickupItem
{
  /// <summary>
  /// Code this key uses to activate keyholes with identical code.
  /// </summary>
  public int keyCode;

  public override void OnPickup(PlayerInventory inv)
  {
    inv.PickItem(this);
    gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
    StartCoroutine(AnimatePickup());
  }
  /// <summary>
  /// Simple particle effect to play on pickup.
  /// </summary>
  /// <returns></returns>
  IEnumerator AnimatePickup()
  {
    var p = gameObject.GetComponent<ParticleSystem>();
    p.Play();
    yield return new WaitForSeconds(p.main.duration);
    Destroy(gameObject);
  }
}

