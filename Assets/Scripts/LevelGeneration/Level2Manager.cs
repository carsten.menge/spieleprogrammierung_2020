﻿using Assets.Scripts.LevelGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Level Manager for Level2. Spawns entities and initializes behaviours.
/// </summary>
public class Level2Manager : MonoBehaviour
{
  public GameManager gm;

  public SpikeTrap spikeTrap;
  public EvilCapsule evilCapsule;

  public List<SpawnPoint> spikeSpawns;
  public List<SpawnPoint> capsuleSpawns;
void Start()
  {
    StartCoroutine(SpawnEntities());
    
  }
  /// <summary>
  /// Coroutine to spawn and initialize the entities. Making sure we wait for FixedUpdate.
  /// </summary>
  IEnumerator SpawnEntities()
  {
    WaitForFixedUpdate delay = new WaitForFixedUpdate();

    foreach (var spikeSpawn in spikeSpawns)
    {
      yield return delay;

      SpikeTrap x = Instantiate(spikeTrap, spikeSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = spikeSpawn;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var capsuleSpawn in capsuleSpawns)
    {
      yield return delay;

      EvilCapsule x = Instantiate(evilCapsule, capsuleSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = capsuleSpawn;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }
    // After everything is spawned, the level is ready.
    StaticLevelInfo.levelReady = true;
  }
}
