﻿

using UnityEngine;
/// <summary>
/// Tile for an item room. Requires a key to be spawned!
/// </summary>
public class ItemRoom : Tile
{
  public Keyhole keyhole;
  public int keyCode;
  void Start()
  {
    keyhole.keyCode = keyCode;
  }
}

