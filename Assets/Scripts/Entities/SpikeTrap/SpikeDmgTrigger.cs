﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Trigger to apply the spike damage.
/// </summary>
public class SpikeDmgTrigger : MonoBehaviour
{
  public SpikeTrap spikeTrap;
  
  void OnTriggerEnter(Collider other)
  {
    IHealth damagableObject;
    if (other.gameObject.TryGetComponent<IHealth>(out damagableObject))
    {
      damagableObject.ApplyDamage(spikeTrap.dmgPerSpike);
    }
  }    
}
