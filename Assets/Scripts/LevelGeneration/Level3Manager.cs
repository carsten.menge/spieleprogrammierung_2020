﻿using Assets.Scripts.Entities;
using Assets.Scripts.LevelGeneration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Level Manager for Level3. Spawns entities and initializes behaviours.
/// </summary>
public class Level3Manager : MonoBehaviour
{
  public GameManager gm;

  public List<SpawnPoint> spikeSpawns;
  public List<SpawnPoint> capsuleSpawns;
  public List<SpawnPoint> turretSpawns;

  public List<SpawnPoint> spinnythingySpawns;
  public List<SpawnPoint> speedpadSpawns;

  public SpikeTrap spikeTrap;
  public EvilCapsule evilCapsule;
  public Turret turret;

  public Spinnythingy spinnythingy;
  public SpeedPad speedPad;

  void Start()
  {
    StartCoroutine(SpawnEntities());
  }
  /// <summary>
  /// Coroutine to spawn and initialize the entities. Making sure we wait for FixedUpdate.
  /// </summary>
  IEnumerator SpawnEntities()
  {
    WaitForFixedUpdate delay = new WaitForFixedUpdate();

    foreach (var spikeSpawn in spikeSpawns)
    {
      yield return delay;

      SpikeTrap x = Instantiate(spikeTrap, spikeSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = spikeSpawn;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var capsuleSpawn in capsuleSpawns)
    {
      yield return delay;

      EvilCapsule x = Instantiate(evilCapsule, capsuleSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = capsuleSpawn;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var turretSpawn in turretSpawns)
    {
      yield return delay;

      Turret x = Instantiate(turret, turretSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = turretSpawn;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var spinnySpawn in spinnythingySpawns)
    {
      yield return delay;

      Spinnythingy x = Instantiate(spinnythingy, spinnySpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = spinnySpawn;
      gm.allEntities.Add(x);
      x.InitializeBehaviour();
    }

    foreach (var speedpadSpawn in speedpadSpawns)
    {
      yield return delay;

      SpeedPad x = Instantiate(speedPad, speedpadSpawn.transform);
      x.GameManager = gm;
      x.SpawnPoint = speedpadSpawn;
      gm.allEntities.Add(x);
      x.InitializeBehaviour();
    }
    // After everything is spawned, the level is ready.
    StaticLevelInfo.levelReady = true;
  }
}

