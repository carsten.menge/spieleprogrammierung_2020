﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Abstract projectile class.
/// </summary>
public abstract class Projectile : MonoBehaviour, IProjectile
{
  /// <summary>
  /// Projectile speed.
  /// </summary>
  public float Speed { get; set; }
  /// <summary>
  /// Direction of flight.
  /// </summary>
  public Vector3 ShootDirection { get; set; }
  public abstract void OnHitEntity(GameEntity target);
  public abstract void OnHitEnvironment(Collider target);
  public abstract void OnHitIHealth(IHealth target);
  /// <summary>
  /// The projectiles Rigidbody.
  /// </summary>
  private Rigidbody r;

  void Start()
  {
    r = GetComponent<Rigidbody>();
  }

  void Update()
  {
    transform.rotation = Quaternion.LookRotation(ShootDirection);
    r.velocity = ShootDirection * Speed;
  }
  void OnTriggerEnter(Collider other)
  {
    if (!other.isTrigger)
    {
      IHealth damageableTarget;
      GameEntity entity;
      // check what kind of object we hit and call the appropriate function
      if (other.TryGetComponent<IHealth>(out damageableTarget))
      {
        OnHitIHealth(damageableTarget);
      }
      else if (other.TryGetComponent<GameEntity>(out entity))
      {
        OnHitEntity(entity);
      }
      else
      {
        OnHitEnvironment(other);
      }
    }
  }
}
