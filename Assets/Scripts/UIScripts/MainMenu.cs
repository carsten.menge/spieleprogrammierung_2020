﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Main Menu controller class
/// </summary>
public class MainMenu : MonoBehaviour
{
  public InputField inputField;
  public Text highscoreText;
  public Slider lvlSizeSlider;
  public Slider numOfLvlSlider;


  void Start()
  {
    // load highscore data, if it exists
    if (File.Exists(Application.persistentDataPath + GameManager.HIGHSCORE_FILENAME)) 
    {
      string s = "";
      string json = File.ReadAllText(Application.persistentDataPath + GameManager.HIGHSCORE_FILENAME);
      HighScoreObject hs = JsonUtility.FromJson<HighScoreObject>(json);
      for (int i = 0; i < hs.times.Length; i++)
      {
        s += $"{i + 1}: {TimeSpan.FromMilliseconds(hs.times[i])}\n";
      }
      highscoreText.text = s;
    } 
  }
  /// <summary>
  /// Play button. Starts the game depending on the selected options.
  /// </summary>
  public void Play()
  {
    if (StaticLevelInfo.preGenLevelPlay)
    {
      StaticLevelInfo.numberOfLevels = 3;
      StaticLevelInfo.levelsPlayed = 0;
      SceneManager.LoadScene(1);
    } else
    {
      StaticLevelInfo.randomLevelSize = (int)lvlSizeSlider.value;
      StaticLevelInfo.numberOfLevels = (int)numOfLvlSlider.value;
      StaticLevelInfo.numberOfEnemies = StaticLevelInfo.randomLevelSize * 2;
      StaticLevelInfo.numberOfOtherEntities = StaticLevelInfo.randomLevelSize * 2;
      StaticLevelInfo.levelsPlayed = 0;
      SceneManager.LoadScene(5);
    }   
  }
  /// <summary>
  /// Quit the game.
  /// </summary>
  public void Quit()
  {
    Application.Quit();
  }
}
