﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Abstract implementation for Items to be picked up by the player.
/// </summary>
public abstract class PickupItem : MonoBehaviour
{ 
  /// <summary>
  /// Executed on pickup of the item.
  /// </summary>
  /// <param name="inv">The inventory of the player.</param>
  public abstract void OnPickup(PlayerInventory inv);
  void OnTriggerEnter(Collider other)
  {
    PlayerInventory inv;
    if (other.TryGetComponent<PlayerInventory>(out inv))
    {
      OnPickup(inv);
    }
  }
}

