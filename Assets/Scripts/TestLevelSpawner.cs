﻿using Assets.Scripts.Entities;
using Assets.Scripts.LevelGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLevelSpawner : MonoBehaviour
{
  public List<SpawnPoint> evilCapsuleSpawner;
  public List<SpawnPoint> spikeTrapSpawner;
  public List<SpawnPoint> speedPadSpawner;
  public List<SpawnPoint> turretSpawner;

  public GameManager gm;

  public EvilCapsule evilCapsule;
  public SpikeTrap spikeTrap;
  public Turret turret;

  public SpeedPad speedPad;

  void Start()
  {
    Invoke("SpawnTestEntities", 2);
  }

  void SpawnTestEntities()
  {
    StartCoroutine(SpawnTestEntitiesCoroutine());
  }

  IEnumerator SpawnTestEntitiesCoroutine()
  {
    WaitForFixedUpdate delay = new WaitForFixedUpdate();

    foreach (var spawnPoint in evilCapsuleSpawner)
    {
      yield return delay;

      EvilCapsule x = Instantiate(evilCapsule, spawnPoint.transform);
      x.GameManager = gm;
      x.SpawnPoint = spawnPoint;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var spawnPoint in spikeTrapSpawner)
    {
      yield return delay;

      SpikeTrap x = Instantiate(spikeTrap, spawnPoint.transform);
      x.GameManager = gm;
      x.SpawnPoint = spawnPoint;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }

    foreach (var spawnPoint in speedPadSpawner)
    {
      yield return delay;

      SpeedPad x = Instantiate(speedPad, spawnPoint.transform);
      x.GameManager = gm;
      x.SpawnPoint = spawnPoint;
      gm.allEntities.Add(x);
      x.InitializeBehaviour();
    }

    foreach (var spawnPoint in turretSpawner)
    {
      yield return delay;

      Turret x = Instantiate(turret, spawnPoint.transform);
      x.GameManager = gm;
      x.SpawnPoint = spawnPoint;
      gm.allEnemies.Add(x);
      x.InitializeEnemyBehaviours();
    }
    StaticLevelInfo.levelReady = true;
  }
}
