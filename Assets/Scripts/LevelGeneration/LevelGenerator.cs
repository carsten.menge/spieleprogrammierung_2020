﻿using Assets.Scripts;
using Assets.Scripts.Entities;
using Assets.Scripts.LevelGeneration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
/// <summary>
/// Level Generator for random level generation. Reads the required data from StaticLevelInfo.
/// </summary>
public class LevelGenerator : MonoBehaviour
{
  public Tile startTilePrefab, endTilePrefab, itemRoomPrefab;
  public Tile[] tilePrefabs;
  public List<Enemy> enemyTypes;
  public List<GameEntity> otherEntityTypes;
  public GameObject plugPrefab;
  public LayerMask groundLayer;
  public GameManager gameManager;
  public GameKey keyPf;

  StartTile startTile;
  EndTile endTile;
  ItemRoom itemRoom;
  List<Tile> tiles = new List<Tile>();

  List<TileConnector> availableConnectors = new List<TileConnector>();
  List<SpawnPoint> availableSpawnPoints = new List<SpawnPoint>();
  List<Enemy> placedEnemies = new List<Enemy>();
  List<GameEntity> placedEntities = new List<GameEntity>();
  GameObject navMeshParent;

  void Start()
  {
    if (navMeshParent == null)
    {
      navMeshParent = new GameObject("NavMeshParent");
    }
    StaticLevelInfo.levelReady = false;
    StartCoroutine(GenerateLevel());
  }
  /// <summary>
  /// Random level generation. 
  /// </summary>
  IEnumerator GenerateLevel()
  {
    // set the delay to the FixedUpdate rate.
    WaitForFixedUpdate delay = new WaitForFixedUpdate();

    // read the data from StaticLevelInfo
    int lvlSize = StaticLevelInfo.randomLevelSize;
    int numberOfEnemies = StaticLevelInfo.numberOfEnemies;
    int numberOfOtherEntities = StaticLevelInfo.numberOfOtherEntities;

    // Place the start tile
    startTile = Instantiate(startTilePrefab) as StartTile;
    startTile.transform.parent = this.transform;
    startTile.transform.position = Vector3.zero;
    startTile.transform.rotation = Quaternion.identity;

    availableConnectors.AddRange(startTile.connectors);
    availableSpawnPoints.AddRange(startTile.possibleSpawnPoints);

    yield return delay;

    // place the other tiles
    while (tiles.Count < lvlSize)
    {
      var j = Random.Range(0, tilePrefabs.Count());
      Tile currTile = Instantiate(tilePrefabs[j]) as Tile;
      currTile.transform.parent = this.transform;
      j = Random.Range(0, availableConnectors.Count());
      TileConnector conn = availableConnectors[j];
      j = Random.Range(0, currTile.connectors.Count());
      TileConnector connCurr = currTile.connectors[j];
      PositionTileAtConnector(currTile, connCurr, conn);

      yield return delay;

      // check if there's an overlap
      if (CheckOverlap(currTile))
      {
        Debug.DrawRay(currTile.transform.position, Vector3.up);
        Destroy(currTile.gameObject);
        availableConnectors.Remove(conn);
        CloseOffTileConnector(conn);
      } else
      {
        tiles.Add(currTile);
        availableConnectors.AddRange(currTile.connectors);
        availableSpawnPoints.AddRange(currTile.possibleSpawnPoints);
        availableConnectors.Remove(conn);
        Destroy(conn);
        availableConnectors.Remove(connCurr);
        Destroy(conn);
      }
    }

    yield return delay;

    // place the item room
    itemRoom = Instantiate(itemRoomPrefab) as ItemRoom;
    itemRoom.transform.parent = this.transform;
    itemRoom.transform.position = Vector3.zero;
    itemRoom.transform.rotation = Quaternion.identity;
    itemRoom.keyCode = 0;

    var i = Random.Range(0, availableConnectors.Count);
    TileConnector itemTileSourceConnector = availableConnectors[i];
    PositionTileAtConnector(itemRoom, itemRoom.connectors[0], itemTileSourceConnector);
    availableConnectors.Remove(itemTileSourceConnector);
    availableSpawnPoints.AddRange(itemRoom.possibleSpawnPoints);

    // spawn the key to the item room and end tile in a random location
    i = Random.Range(0, availableSpawnPoints.Count);
    SpawnPoint keySpawnPoint = availableSpawnPoints[i];
    GameKey k = Instantiate(keyPf, keySpawnPoint.transform) as GameKey;
    k.keyCode = itemRoom.keyCode;
    availableSpawnPoints.Remove(keySpawnPoint);

    yield return delay;

    // place the end tile on one of the available connectors and close of all remaining ones
    endTile = Instantiate(endTilePrefab) as EndTile;
    endTile.transform.parent = this.transform;
    endTile.transform.position = Vector3.zero;
    endTile.transform.rotation = Quaternion.identity;

    i = Random.Range(0, availableConnectors.Count);
    TileConnector endTileSourceConnector = availableConnectors[i]; 
    PositionTileAtConnector(endTile, endTile.connectors[0], endTileSourceConnector);
    availableConnectors.Remove(endTileSourceConnector);
    availableSpawnPoints.AddRange(endTile.possibleSpawnPoints);
    foreach (TileConnector conn in availableConnectors)
    {
      CloseOffTileConnector(conn);
    }

    // create the navmesh
    foreach (var tile in tiles)
    {
      tile.navMeshSurface.BuildNavMesh();
    }

    // with the level geometry placed and the navmesh created, place the enemies
    while (placedEnemies.Count < numberOfEnemies)
    {
      yield return delay;

      // in case we run out of spawn points, break
      if (availableSpawnPoints.Count <= 0) break;

      i = Random.Range(0, availableSpawnPoints.Count);
      SpawnPoint spawnPoint = availableSpawnPoints[i];
      Enemy enemy = InstantiateRandomEnemy(spawnPoint);
      enemy.SpawnPoint = spawnPoint;

      availableSpawnPoints.Remove(spawnPoint);
      placedEnemies.Add(enemy);

      enemy.InitializeEnemyBehaviours();
      enemy.GameManager = gameManager;
    }
    // when we're done, add the list of enemies to the gameManager
    gameManager.allEnemies.AddRange(placedEnemies);

    // place other game entities
    while (placedEntities.Count < numberOfOtherEntities)
    {
      yield return delay;

      if (availableSpawnPoints.Count <= 0) break;

      i = Random.Range(0, availableSpawnPoints.Count); 
      SpawnPoint spawnPoint = availableSpawnPoints[i];
      i = Random.Range(0, otherEntityTypes.Count);
      GameEntity entity = Instantiate(otherEntityTypes[i]);

      entity.GameManager = gameManager;
      entity.SpawnPoint = spawnPoint;
      entity.InitializeBehaviour();
      availableSpawnPoints.Remove(spawnPoint);

      placedEntities.Add(entity);
    }
    gameManager.allEntities.AddRange(placedEntities);

    StaticLevelInfo.levelReady = true;
  }
  /// <summary>
  /// Method to posision a tile so that it properly touches the connector of another.
  /// </summary>
  /// <param name="tile">Tile to connect.</param>
  /// <param name="tileConn">The connector on the tile to connect.</param>
  /// <param name="sourceConn">The connector to attach the tile to.</param>
  void PositionTileAtConnector(Tile tile, TileConnector tileConn, TileConnector sourceConn)
  {
    // Get the angle between the connectors and rotate the new tile by that angle
    float deltaAngle = Mathf.DeltaAngle(tileConn.transform.rotation.eulerAngles.y, sourceConn.transform.rotation.eulerAngles.y);
    tile.transform.Rotate(Vector3.up, deltaAngle);
    tile.transform.Rotate(Vector3.up, 180f);

    Vector3 tilePositionOffset = tileConn.transform.position - tile.transform.position;
    tile.transform.position = sourceConn.transform.position - tilePositionOffset;

    Destroy(tileConn.gameObject);
  }
  /// <summary>
  /// Method to close off a TileConnector.
  /// </summary>
  /// <param name="conn">Connector to close.</param>
  void CloseOffTileConnector(TileConnector conn)
  {
    GameObject plug = Instantiate(plugPrefab);
    plug.transform.position = conn.transform.position;
    plug.transform.rotation = conn.transform.rotation;
    Destroy(conn.gameObject);
  }
  /// <summary>
  /// Method to check for and correct overlapping tiles.
  /// </summary>
  /// <param name="tile">Tile to check.</param>
  /// <returns>True if there is an overlap.</returns>
  bool CheckOverlap(Tile tile) 
  {
    // subtract a little epsilon from the bounding box, so tiles may touch
    Bounds bounds = tile.TileBounds();
    bounds.Expand(-.01f);

    Collider[] colliders = Physics.OverlapBox(bounds.center, bounds.size / 2, tile.transform.rotation);
    if (colliders.Length > 0)
    {
      foreach (var coll in colliders)
      {
        if (coll.transform.parent.gameObject.Equals(tile.gameObject))
        {
          continue;
        } else
        {
          Debug.Log("there is an overlap, tile cannot be placed here");
          return true;
        }
      }
    }
    return false;
  }
  /// <summary>
  /// Instantiate a random Enemy at a spawn point.
  /// </summary>
  /// <param name="spawnPoint">Spawn point for the enemy.</param>
  /// <returns></returns>
  public Enemy InstantiateRandomEnemy(SpawnPoint spawnPoint)
  {
    var i = Random.Range(0, enemyTypes.Count);
    return Instantiate<Enemy>(enemyTypes[i], spawnPoint.transform); 
  }
}
