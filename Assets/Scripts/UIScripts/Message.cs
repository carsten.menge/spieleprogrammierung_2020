﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.UIScripts
{
  /// <summary>
  /// Message to be displayed to the player. Has text and duration.
  /// </summary>
  public class Message
  {
    public string Text { get; set; }
    public long Duration { get; set; }
    public Message(string text, long duration)
    {
      this.Text = text;
      this.Duration = duration;
    }
  }
}