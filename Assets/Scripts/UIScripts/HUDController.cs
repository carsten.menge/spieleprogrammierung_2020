﻿using Assets.Scripts;
using Assets.Scripts.UIScripts;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
/// <summary>
/// Controller for the Game HUD.
/// </summary>
public class HUDController : MonoBehaviour, IGameStateListener
{
  public GameManager gameManager;
  public Text timer;
  public Text hudMainText;
  public Button continueButton;
  public Text continueButtonText;
  public Button toMenuButton;
  public Text toMenuButtonText;
  public Text healthText;
  public Image healthImage;
  public Canvas loadingScreen;
  public Image keyIndicator;
  
  private PlayerController player;
  private GameState gameState;

  private Queue<Message> messageQueue = new Queue<Message>();
  private bool messagePlaying = false;

  public void OnUpdateGameState(GameState gameState)
  {
    this.gameState = gameState;

    // activate the loading screen if the level is not finished yet.
    if (gameState.State == GameState.States.LevelLoading)
    {
      loadingScreen.gameObject.SetActive(true);
    } else
    {
      loadingScreen.gameObject.SetActive(false);
    }

    // make sure the buttons are only on if appropriate,
    // I feel like I should handle this differently...
    bool buttonsOn = gameState.State == GameState.States.GameWon || gameState.State == GameState.States.GameLost;
    continueButton.gameObject.SetActive(buttonsOn);
    toMenuButton.gameObject.SetActive(buttonsOn);

    // don't display health if we're on the lost or won screen, i.e. when buttonsOn is true
    if (buttonsOn)
    {
      healthText.gameObject.SetActive(false);
      healthImage.gameObject.SetActive(false);
    } else
    {
      healthText.gameObject.SetActive(true);
      healthImage.gameObject.SetActive(true);

    }

    if (gameState.State == GameState.States.PreStart)
    {
      hudMainText.gameObject.SetActive(true);
      hudMainText.text = "Kugel bewegen mit WASD,\nbewegen um das Spiel zu starten.";
    }
    if (gameState.State == GameState.States.GameRunning)
    {
      hudMainText.enabled = false;
    } else if (gameState.State == GameState.States.GameWon) 
    {
      hudMainText.gameObject.SetActive(true);
      hudMainText.text = "Ziel erreicht in: " + gameState.stopwatch.Elapsed.ToString();

      continueButtonText.text = "Weiter";

      toMenuButtonText.text = "Zum Menu";

    } else if (gameState.State == GameState.States.GameLost)
    {
      hudMainText.gameObject.SetActive(true);
      hudMainText.text = "Leider verloren";

      continueButtonText.text = "Nochmal versuchen";

      toMenuButtonText.text = "Zum Menu";
    }

  }
  void Start()
  {
    gameManager.Subscribe(this);
    player = gameManager.playerMarble;
    this.gameState = gameManager.GetCurrentGameState();
    hudMainText.enabled = true;
    hudMainText.text = "Kugel bewegen mit WASD,\nbewegen um das Spiel zu starten.";
  }

  void Update()
  {
    timer.text = gameState.stopwatch.Elapsed.ToString();

    healthText.text = ((int)player.Health).ToString();
    if (player.Health <= 25)
    {
      healthImage.color = Color.red;
      healthText.color = Color.red;
    }
    else
    {
      healthImage.color = Color.white;
      healthText.color = Color.white;
    }

    if (player.inventory.keys.Count > 0)
    {
      keyIndicator.color = Color.green;
    }
    else
    {
      keyIndicator.color = Color.red;
    }

    if (!messagePlaying && messageQueue.Count > 0) { StartCoroutine(DisplayMsg(messageQueue.Dequeue())); }
  }

  public void ContinueButtonOnClick()
  {
    gameManager.ContinueGame();
  }

  public void ToMenuButtonOnClick()
  {
    gameManager.EndGame();
  }
  /// <summary>
  /// Puts a message to be displayed to the player on the queue. Messages play one after another.
  /// </summary>
  /// <param name="message">Message text</param>
  /// <param name="duration">Duration to display in seconds</param>
  public void DisplayMsgToPlayer(string message, long duration)
  {
    messageQueue.Enqueue(new Message(message, duration));
  }

  private IEnumerator DisplayMsg(Message message)
  {
    messagePlaying = true;
    hudMainText.text = message.Text;
    hudMainText.enabled = true;
    yield return new WaitForSeconds(message.Duration);
    messagePlaying = false;
    hudMainText.enabled = false;
  }
}
