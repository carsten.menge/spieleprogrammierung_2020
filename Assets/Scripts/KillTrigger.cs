﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Simple trigger to kill the player
/// </summary>
public class KillTrigger : MonoBehaviour
{
  void OnTriggerEnter(Collider other)
  {
    IHealth damageableObject;
    if (other.gameObject.TryGetComponent<IHealth>(out damageableObject))
    {
      damageableObject.Health = 0;
    }
  }
}
