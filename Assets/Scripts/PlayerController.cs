﻿using Assets.Scripts;
using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;
/// <summary>
/// Player Controller contains all the code to control the player's movements.
/// </summary>
public class PlayerController : MonoBehaviour, IGameStateListener, IHealth
{
  private Rigidbody rbody;
  private SphereCollider col;
  private Renderer rend;
  private GameState currentGameState;

  private LayerMask grndLayer;

  public float accForceMultiplier = 350f;
  public float jumpForce = 10f;
  public GameManager gameManager;
  public float maxHealth = 100;

  public bool firstMove = false;

  public float Health { get; set; }
  public PlayerInventory inventory;
  private IWeapon weapon;
  private CameraControl mainCamera;


  // Start is called before the first frame update
  void Start()
  {
    mainCamera = gameManager.mainCamera.GetComponent<CameraControl>();
    rbody = GetComponent<Rigidbody>();
    col = GetComponent<SphereCollider>();
    rend = GetComponent<Renderer>();
    grndLayer = LayerMask.GetMask("Ground");
    gameManager.Subscribe(this);
    currentGameState = gameManager.GetCurrentGameState();
    Health = maxHealth;
  }

  void Update()
  {
    // Check if we need to start the timer
    if (!firstMove && (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0))
    {
      firstMove = true;
    }

    if (currentGameState.State == GameState.States.GameRunning)
    {
      // show green if we can control the marble
      if (SitsAtopGrndLayer()) rend.material.SetColor("_Color", Color.green);
      else rend.material.SetColor("_Color", Color.red);      

      // control according to the selected control scheme
      if (SitsAtopGrndLayer() && gameManager.absoluteControlScheme)
      {
        rbody.AddForce(Vector3.forward * Input.GetAxis("Vertical") * Time.deltaTime * accForceMultiplier, ForceMode.Force);
        rbody.AddForce(Vector3.right * Input.GetAxis("Horizontal") * Time.deltaTime * accForceMultiplier, ForceMode.Force);
      } else if (SitsAtopGrndLayer())
      {
        Vector3 camForward = new Vector3(mainCamera.transform.forward.x, 0, mainCamera.transform.forward.z).normalized;
        Vector3 camRight = mainCamera.transform.right;
        rbody.AddForce(camForward * Input.GetAxis("Vertical") * Time.deltaTime * accForceMultiplier, ForceMode.Force);
        rbody.AddForce(camRight * Input.GetAxis("Horizontal") * Time.deltaTime * accForceMultiplier, ForceMode.Force);
      }

      if (SitsAtopGrndLayer() && Input.GetKeyDown(KeyCode.Space))
      {
        rbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
      }

      // control shooting
      if (weapon != null)
      {
        // offset so projectiles don't spawn inside the player
        float offset = 1f;
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        {
          Vector3 direction = new Vector3();
          // if in absolute control mode, we just shoot in the cardinal directions
          if (gameManager.absoluteControlScheme)
          {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
              direction = Vector3.forward;
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
              direction = Vector3.left;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
              direction = Vector3.right;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
              direction = Vector3.back;
            }
          } else // else, if in relative control mode, shoot relative to the camera direction.
          {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
              direction = new Vector3(mainCamera.transform.forward.x, 0, mainCamera.transform.forward.z).normalized;
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
              direction = new Vector3(-mainCamera.transform.right.x, 0, -mainCamera.transform.right.z).normalized;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
              direction = new Vector3(mainCamera.transform.right.x, 0, mainCamera.transform.right.z).normalized;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
              direction = new Vector3(-mainCamera.transform.forward.x, 0, -mainCamera.transform.forward.z).normalized;
            }
          }

          Projectile p = Instantiate(weapon.Projectile, transform.position + direction * offset, Quaternion.identity);
          p.Speed = weapon.ProjectileSpeed;
          p.ShootDirection = direction;
        }
        
      } else
      {
        // TODO: temporary solution
        // see if we got one, then equip it
        List<IWeapon> w = inventory.Weapons();
        if (w.Count > 0) weapon = w[0];       
      }

    } else { rend.material.SetColor("_Color", Color.red); }
  }
  /// <summary>
  /// Check if the player sits atop a controllable ground layer.
  /// </summary>
  /// <returns>True if the player is on ground.</returns>
  private bool SitsAtopGrndLayer()
  {
    return Physics.Raycast(rbody.transform.position, Vector3.down, col.radius + 0.1f, grndLayer);
  }

  public void EnteredGoalTrigger()
  {
    gameManager.PlayerEnteredGoalTrigger();  
  }

  public void OnUpdateGameState(GameState gameState)
  {
    currentGameState = gameState;
  }
  /// <summary>
  /// Resets the player marble to not moving and restores health to full.
  /// </summary>
  public void ResetMarble()
  {
    rbody.velocity = Vector3.zero;
    rbody.angularVelocity = Vector3.zero;
    firstMove = false;
    Health = maxHealth;
  }

  public bool IsAlive()
  {
    return Health > 0;
  }

  public void ApplyDamage(float dmgToApply)
  {
    Health -= dmgToApply;
  }

  public void Heal(float amountToHeal)
  {
    Health += amountToHeal;
  }
}
