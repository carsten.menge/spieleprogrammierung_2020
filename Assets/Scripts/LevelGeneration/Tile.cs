﻿using Assets.Scripts.LevelGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/// <summary>
/// General Tile class.
/// </summary>
public class Tile : MonoBehaviour
{
  /// <summary>
  /// A list of all connectors this tile exposes.
  /// </summary>
  public List<TileConnector> connectors;
  /// <summary>
  /// The tile's collider.
  /// </summary>
  public MeshCollider meshCollider;
  /// <summary>
  /// A list of spawn points for entities the tile exposes.
  /// </summary>
  public List<SpawnPoint> possibleSpawnPoints;
  /// <summary>
  /// The tiles nav mesh surface.
  /// </summary>
  public NavMeshSurface navMeshSurface;
  /// <summary>
  /// Returns the bounds of the tile.
  /// </summary>
  /// <returns>Bounds of the tile</returns>
  public Bounds TileBounds() { return meshCollider.bounds; }
}
