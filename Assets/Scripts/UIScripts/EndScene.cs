﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// End screen that displays the total times.
/// </summary>
public class EndScene : MonoBehaviour
{
  public Text endScreenList;

  void Start()
  {
    string s = "";
    long total = 0;
    for (int i = 0; i < StaticLevelInfo.times.Count; i++)
    {
      s += $"Round {i + 1}: {TimeSpan.FromMilliseconds(StaticLevelInfo.times[i])}\n";
      total += StaticLevelInfo.times[i];
    }
    s += $"Total: {TimeSpan.FromMilliseconds(total)}\n";

    endScreenList.text = s;
  }
}
