﻿using Assets.Scripts;
using Assets.Scripts.LevelGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Level Manager for Level1. Spawns entities and initializes behaviours.
/// </summary>
public class Level1Manager : MonoBehaviour
{
  public HUDController hud;
  public GameManager gameManager;

  public SpikeTrap spikeTrap;
  public EvilCapsule evilCapsule;

  public List<SpawnPoint> spikeSpawns;
  public List<SpawnPoint> capsuleSpawns;

  void Start()
  {
    StartCoroutine(SpawnEntities());
  }
  /// <summary>
  /// Coroutine to spawn and initialize the entities. Making sure we wait for FixedUpdate.
  /// </summary>
  IEnumerator SpawnEntities()
  {
    WaitForFixedUpdate delay = new WaitForFixedUpdate();

    foreach (var trapSpawn in spikeSpawns)
    {
      yield return delay;

      SpikeTrap s = Instantiate(spikeTrap, trapSpawn.transform);
      s.GameManager = gameManager;
      s.SpawnPoint = trapSpawn;
      gameManager.allEnemies.Add(s);
      s.InitializeEnemyBehaviours();
    }

    foreach (var capsuleSpawn in capsuleSpawns)
    {
      yield return delay;

      EvilCapsule c = Instantiate(evilCapsule, capsuleSpawn.transform);
      c.GameManager = gameManager;
      c.SpawnPoint = capsuleSpawn;
      gameManager.allEnemies.Add(c);
      c.InitializeEnemyBehaviours();
    }
    // after everything is spawned, the level is ready.
    StaticLevelInfo.levelReady = true;
  }

}
