﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// SpeedPad
/// Gives the Player a speed boost in the direction it is pointing as the player moves over it.
/// </summary>
namespace Assets.Scripts.Entities
{
  public class SpeedPad : GameEntity
  {
    public float speedBoost = 10.0f;
    Vector3 direction;
    void Start()
    {
      direction = transform.forward;
    }

    void OnTriggerStay(Collider other)
    {
      Rigidbody rBody;
      if (other.gameObject.TryGetComponent<Rigidbody>(out rBody))
      {
        rBody.AddForce(direction * speedBoost, ForceMode.Force);
      }
    }

    public override void InitializeBehaviour()
    {
      transform.position = SpawnPoint.transform.position;
      transform.Translate(Vector3.up * 0.01f);
      transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
    }
  }
}
