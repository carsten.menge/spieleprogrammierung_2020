﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Simple Turret
/// In Dormant state, the turret turns 360 degrees. When the player enters the viewcone, it follows and shoots. 
/// Returns to dormant after player leaves viewcone.
/// </summary>
public class Turret : Enemy
{
  public Collider viewCone;
  public GameObject moveableParts;
  public float rotationSpeed = 24f;
  public float shootInterval = 1f;
  public GameObject shootyPoint;
  public TurretWeapon weapon;

  public Mode State { get; set; }
  public GameObject TargetGameObject { get; set; }

  public enum Mode
  {
    Searching,
    Attacking
  }
  

  public override void InitializeEnemyBehaviours()
  {
    transform.position = SpawnPoint.transform.position;
    State = Mode.Searching;
    StartCoroutine(IdleRotation());
  }

  bool LookingAtTarget(GameObject target)
  {
    if (target == null) return false;

    Vector3 directionToTarget = (target.transform.position - transform.position).normalized;
    Ray targetRay = new Ray(shootyPoint.transform.position, directionToTarget);
    RaycastHit hit;
    if (Physics.Raycast(targetRay, out hit)) 
    {
      return true;
    }
    return false;
  }

  IEnumerator IdleRotation()
  {
    while (true)
    {
      moveableParts.transform.Rotate(Vector3.up * rotationSpeed * Time.fixedDeltaTime);
      yield return new WaitForFixedUpdate();
    }
  }

  IEnumerator RotateTowardsTarget()
  {
    while (true)
    {
      if (TargetGameObject != null)
      {
        Quaternion directionToTarget = Quaternion.LookRotation((TargetGameObject.transform.position - transform.position).normalized);
        Quaternion rotation = moveableParts.transform.rotation;
        moveableParts.transform.rotation = Quaternion.Lerp(rotation, directionToTarget, 1f * Time.fixedDeltaTime);
        moveableParts.transform.rotation = Quaternion.Euler(new Vector3(0f, moveableParts.transform.rotation.eulerAngles.y, 0f));
        yield return new WaitForFixedUpdate();
      } else
      {
        yield return new WaitForFixedUpdate();
      }
    }
  }

  IEnumerator Shoot()
  {
    while (true)
    {
      if (LookingAtTarget(TargetGameObject))
      {
        Projectile p = Instantiate(weapon.Projectile, shootyPoint.transform.position, Quaternion.identity);
        p.Speed = weapon.ProjectileSpeed;
        p.ShootDirection = shootyPoint.transform.forward;
      }
      yield return new WaitForSeconds(shootInterval);
    }
  }

  public void UpdateState(Mode state)
  {
    State = state;
    switch (State)
    {
      case Mode.Searching:
        StopAllCoroutines();
        StartCoroutine(IdleRotation());
        break;
      case Mode.Attacking:
        StopAllCoroutines();
        StartCoroutine(RotateTowardsTarget());
        StartCoroutine(Shoot());
        break;
    }
  }
}
