﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileConnector : MonoBehaviour
{
/// <summary>
/// Tile Connector
/// </summary>
  void OnDrawGizmos()
  {
    // to make them easier to place on new Tiles
    Gizmos.color = Color.red;
    Gizmos.DrawRay(transform.position, transform.rotation * Vector3.forward);
    Gizmos.DrawRay(transform.position, transform.rotation * Vector3.right * 5);
    Gizmos.DrawRay(transform.position, transform.rotation * Vector3.left * 5);
  }
}
