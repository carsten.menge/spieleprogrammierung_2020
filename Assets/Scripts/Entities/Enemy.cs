﻿using Assets.Scripts.LevelGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Abstract class defining an Enemy Entity.
/// </summary>
public abstract class Enemy : MonoBehaviour
  {
    public GameManager GameManager { get; set; }
    public SpawnPoint SpawnPoint { get; set; }
  /// <summary>
  /// An abstract method to initialize any behaviours for the concrete enemy, such as states etc.
  /// </summary>
    public abstract void InitializeEnemyBehaviours();
  }
