﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Abstract Button class to implement a button/keyhole or similar, that other objects can subscribe to be notified when it activates.
/// </summary>
public abstract class GameButton : MonoBehaviour
{
  /// <summary>
  /// Subscribe to be notified, when the button is activated.
  /// </summary>
  /// <param name="listener">Listener that implements the IGameButtonListener interface.</param>
  public abstract void Subscribe(IGameButtonListener listener);
  /// <summary>
  /// What to do when the button activates (e.g. notify listeners).
  /// </summary>
  public abstract void Activate();
}
