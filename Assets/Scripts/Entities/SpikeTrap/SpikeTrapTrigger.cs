﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Trigger to activate the Spike Trap
/// </summary>
public class SpikeTrapTrigger : MonoBehaviour
{
  public SpikeTrap spikeTrap;

  void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.name == "PlayerMarble")
    {
      spikeTrap.PlayerEnteredSpikeTrigger();
    }
  }
}
