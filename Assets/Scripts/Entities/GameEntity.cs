﻿using Assets.Scripts.LevelGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Entities
{
  /// <summary>
  /// Abstract class for general game entities.
  /// </summary>
  public abstract class GameEntity : MonoBehaviour
  {
    public GameManager GameManager { get; set; }
    public SpawnPoint SpawnPoint { get; set; }
    /// <summary>
    /// Initialize Entity behaviours.
    /// </summary>
    public abstract void InitializeBehaviour();
  }
}
