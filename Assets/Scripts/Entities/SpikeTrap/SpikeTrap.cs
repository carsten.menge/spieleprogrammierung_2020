﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Simple Spike Trap
/// Spikes spring from the ground shortly before the player would touch them. Deal contact damage.
/// </summary>
  public class SpikeTrap : Enemy
  {
    public float dmgPerSpike = 10f;
    public float extensionSpeed = 15f;
    private bool triggered = false;
    private bool extended = false;
    private float originalPositionY;

    public override void InitializeEnemyBehaviours()
    {
      triggered = false;
      extended = false;
      transform.Translate(Vector3.down * .49f);
      originalPositionY = transform.position.y;
    }

    void Update()
    {
      if (triggered && !extended)
      {
        float move = extensionSpeed * Time.deltaTime;
        transform.Translate(0, move, 0);
        if (transform.position.y >= originalPositionY + .48f)
        {
          extended = true;
        }
      }
    }

    public void PlayerEnteredSpikeTrigger()
    {
      if (!triggered)
      {
        triggered = true;
      }
    }
  }

