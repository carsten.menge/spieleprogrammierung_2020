﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Entities
{
  /// <summary>
  /// Interface for any entity that has a health bar.
  /// </summary>
  public interface IHealth
  {
    /// <summary>
    /// The health value.
    /// </summary>
    float Health { get; set; }
    /// <summary>
    /// A method to check if the entity is alive. Mostly health > 0.
    /// </summary>
    /// <returns>Boolean</returns>
    bool IsAlive();
    /// <summary>
    /// Applies an ampount of damage to the entity.
    /// </summary>
    /// <param name="dmgToApply">Amount of damage to apply as float.</param>
    void ApplyDamage(float dmgToApply);
    /// <summary>
    /// Applies an amount of healing to the entity.
    /// </summary>
    /// <param name="amountToHeal">Amount of healing to apply as float.</param>
    void Heal(float amountToHeal);

  }
}
