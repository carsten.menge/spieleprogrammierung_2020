﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Static Level Info class. Contains all the necessary global information to generate a level and decide what level to play next.
/// </summary>
static class StaticLevelInfo
{
  /// <summary>
  /// Index of main menu level
  /// </summary>
  public const int MENULEVELINDEX = 0;
  /// <summary>
  /// Index of the level endscreen
  /// </summary>
  public const int ENDSCREENLEVELINDEX = 4; 
  public static bool levelReady;
  public static int randomLevelSize;
  public static int numberOfLevels;
  public static int levelsPlayed;
  public static int numberOfEnemies;
  public static int numberOfOtherEntities;
  public static List<long> times;
  public static bool preGenLevelPlay;
}

