﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A simple door that slides down into the ground on activation via a GameButton.
/// </summary>
public class Door : MonoBehaviour, IGameButtonListener
{
  public GameObject door;
  public GameButton button;
  public bool open = false;
  public float speed = 0.1f;

  private bool openTheDoor = false;
  private Transform originalPosition;
  private Bounds bounds;

  void Start()
  {
    bounds = GetComponentInChildren<Renderer>().bounds;
    originalPosition = transform;
    if (open)
    {
      door.transform.Translate(Vector3.down * bounds.extents.y * 2);
    }
    button.Subscribe(this);
  }

  void Update()
  {
    if (openTheDoor && !open)
    {
      door.transform.position += Vector3.down * Time.deltaTime;
      if (door.transform.position.y <= (originalPosition.position.y - (bounds.extents.y * 4)))
      {
        openTheDoor = false;
        open = true;
      }
    }
  }

  public void OpenDoor() 
  {
    openTheDoor = true;
  }

  public void OnButtonActivate(object args)
  {
    OpenDoor();
  }

}
