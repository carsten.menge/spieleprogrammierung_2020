﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Weapon interface. Defines a projectile used and it's projectile speed. 
/// Mostly unused.
/// </summary>
public interface IWeapon
{
  Projectile Projectile { get; }
  float ProjectileSpeed { get; }
}
