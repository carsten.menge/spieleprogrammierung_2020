﻿using Assets.Scripts;
using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
/// <summary>
/// Game manager class. Responsible for distributing game state information to other systems and generally controlling game flow.
/// </summary>
public class GameManager : MonoBehaviour
{
  public const string HIGHSCORE_FILENAME = "/highscore.json";
  public Camera mainCamera;
  public PlayerController playerMarble;
  public float looseHeight = -20f;
  public bool absoluteControlScheme;

  public PlayerSpawn playerSpawn;
  public List<Enemy> allEnemies = new List<Enemy>();
  public List<GameEntity> allEntities = new List<GameEntity>();

  private GameState currentGameState;
  private List<IGameStateListener> gameStateListeners = new List<IGameStateListener>();
  private string highscoreFilePath;

  public void Awake()
  {
    highscoreFilePath = Application.persistentDataPath + HIGHSCORE_FILENAME;
    currentGameState = new GameState();

    // if we are awakening on the first level of a playthrough, start a new times list
    if (StaticLevelInfo.levelsPlayed == 0)
    {
      StaticLevelInfo.times = new List<long>();
    }
    if (StaticLevelInfo.preGenLevelPlay && StaticLevelInfo.levelsPlayed == 0)
    {
      StaticLevelInfo.times = new List<long>();
    }
  }

  void Update()
  {
    // is the level loaded?
    if (currentGameState.State == GameState.States.LevelLoading)
    {
      if (StaticLevelInfo.levelReady)
      {
        currentGameState.State = GameState.States.PreStart;
        NotifyListeners();
      } 
    }
    // have we moved?
    if (currentGameState.State == GameState.States.PreStart && playerMarble.firstMove)
    {
      currentGameState.State = GameState.States.GameRunning;
      currentGameState.stopwatch.Start();

      NotifyListeners();
    }
    // have we lost by falling off the map?
    if (currentGameState.State == GameState.States.GameRunning && playerMarble.transform.position.y < looseHeight)
    {
      currentGameState.State = GameState.States.GameLost;
      currentGameState.stopwatch.Stop();

      NotifyListeners();
    }
    // have we lost by running out of health?
    if (currentGameState.State == GameState.States.GameRunning && !playerMarble.IsAlive())
    {
      currentGameState.State = GameState.States.GameLost;
      currentGameState.stopwatch.Stop();

      NotifyListeners();
    }

    // change control scheme
    if (Input.GetKeyDown("c"))
    {
      absoluteControlScheme = !absoluteControlScheme;
    }

    // a brutal way out
    if (Input.GetKeyDown("escape"))
    {
      SceneManager.LoadScene(StaticLevelInfo.MENULEVELINDEX);
    }
  }
  /// <summary>
  /// To be called when the player enters the goal of a level. 
  /// </summary>
  public void PlayerEnteredGoalTrigger()
  {
    if (currentGameState.State == GameState.States.GameRunning)
    {
      currentGameState.State = GameState.States.GameWon;
      currentGameState.stopwatch.Stop();
      NotifyListeners();
    }
  }
  /// <summary>
  /// Subscribe to the gameManager for gameState updates
  /// </summary>
  /// <param name="listener">The listener to subscribe to the GameManager</param>
  public void Subscribe(IGameStateListener listener)
  {
    gameStateListeners.Add(listener);  
  }
  /// <summary>
  /// Called to update the game state for all subscribed listeners
  /// </summary>
  public void NotifyListeners()
  {
    Debug.Log($"State changed: \n{currentGameState.State}\n{currentGameState.stopwatch}");
    foreach (var listener in gameStateListeners)
    {
      listener.OnUpdateGameState(currentGameState);
    }
  }
  /// <summary>
  /// Get the current GameState
  /// </summary>
  /// <returns>Current GameState</returns>
  public GameState GetCurrentGameState()
  {
    return this.currentGameState;
  }
  /// <summary>
  /// Called to end the game and return to the menu.
  /// </summary>
  public void EndGame()
  {
    SceneManager.LoadScene(0);
  }
  /// <summary>
  /// Called to continue the game after either dying or reaching the goal
  /// </summary>
  public void ContinueGame()
  {
    if (currentGameState.State == GameState.States.GameLost)
    {
      // Load the current level again
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    } else if (currentGameState.State == GameState.States.GameWon)
    {
      StaticLevelInfo.levelsPlayed++;
      // load the next level or end screen if all levels are done
      if (StaticLevelInfo.levelsPlayed == StaticLevelInfo.numberOfLevels)
      {
        StaticLevelInfo.times.Add(currentGameState.stopwatch.ElapsedMilliseconds);
        WriteHighscores();
        SceneManager.LoadScene(StaticLevelInfo.ENDSCREENLEVELINDEX);
      } else if (!StaticLevelInfo.preGenLevelPlay)
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        StaticLevelInfo.times.Add(currentGameState.stopwatch.ElapsedMilliseconds);
      } else
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        StaticLevelInfo.times.Add(currentGameState.stopwatch.ElapsedMilliseconds);
      }
    }
  }
  
  /// <summary>
  /// Writes the new highscores to disk.
  /// </summary>
  private void WriteHighscores()
  {
    if (!StaticLevelInfo.preGenLevelPlay) return;
    HighScoreObject hs;
    List<long> times;
    if (File.Exists(highscoreFilePath))
    {
      hs = JsonUtility.FromJson<HighScoreObject>(File.ReadAllText(highscoreFilePath));
      times = new List<long>(hs.times);
    } else
    {
      hs = new HighScoreObject();
      times = new List<long>();
      File.Create(highscoreFilePath).Close();
    }

    times.Add(StaticLevelInfo.times.Sum());
    times.Sort();
    hs.times = times.GetRange(0, Math.Min(5, times.Count)).ToArray();

    string json = JsonUtility.ToJson(hs);
    File.WriteAllText(highscoreFilePath, json);
  }
} 
