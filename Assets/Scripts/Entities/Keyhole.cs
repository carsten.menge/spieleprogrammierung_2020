﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Special GameButton that can only be activated with a key with the correct key code.
/// </summary>
public class Keyhole : GameButton
{
  public bool isActivated;
  public int keyCode = 0;

  private List<IGameButtonListener> listeners = new List<IGameButtonListener>();

  void OnTriggerEnter(Collider other)
  {
    PlayerInventory inv;
    if (other.gameObject.TryGetComponent<PlayerInventory>(out inv))
    {
      if (inv.HasKeyWithCode(keyCode)) Activate();
    }
  }

  public override void Subscribe(IGameButtonListener listener)
  {
    listeners.Add(listener);
  }

  public override void Activate()
  {
    listeners.ForEach(l => l.OnButtonActivate(null));
    isActivated = !isActivated;
  }
}

