﻿
using System.Runtime.CompilerServices;
using UnityEngine;
/// <summary>
/// Weapon Class for the turret
/// </summary>
public class TurretWeapon : MonoBehaviour, IWeapon
{
  public Projectile projectile;
  public float projectileSpeed;
  public Projectile Projectile => projectile;
  public float ProjectileSpeed => projectileSpeed;

}
