﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
/// <summary>
/// Simple implementation of a player inventory.
/// </summary>
public class PlayerInventory : MonoBehaviour
{
  public List<GameKey> keys = new List<GameKey>();

  private List<PickupItem> allItems = new List<PickupItem>();
  private List<IWeapon> weapons = new List<IWeapon>();
  /// <summary>
  /// Called to add an item to the inventory.
  /// </summary>
  /// <param name="item">The item to add</param>
  public void PickItem(PickupItem item)
  {
    allItems.Add(item);

    // handle specific types of items
    if (item is GameKey)
    {
      keys.Add(item as GameKey);
    }

    if (item is IWeapon)
    {
      weapons.Add(item as IWeapon);
    }
  }
  /// <summary>
  /// Check if the player inventory contains a key with a certain code.
  /// </summary>
  /// <param name="keyCode">The code to check for</param>
  /// <returns>True if the inventory contains a key with the correct code</returns>
  public bool HasKeyWithCode(int keyCode)
  {
    foreach (var key in keys)
    {
      if (key.keyCode == keyCode) return true;
    }

    return false;
  }

  public List<IWeapon> Weapons() { return weapons; }
}

