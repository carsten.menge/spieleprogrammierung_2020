﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Options Menu. 
/// </summary>
public class OptionsMenu : MonoBehaviour
{
  private Toggle randomLevelToggle;
  public Text[] labels;
  public Slider[] sliders;

  void Start()
  {
    randomLevelToggle = GetComponent<Toggle>();
    randomLevelToggle.isOn = !StaticLevelInfo.preGenLevelPlay;
    randomLevelToggle.onValueChanged.AddListener(delegate { ToggleValueChanged(randomLevelToggle); });
    ToggleValueChanged(randomLevelToggle);
  }
 
  void ToggleValueChanged(Toggle toggle) 
  {
    StaticLevelInfo.preGenLevelPlay = !toggle.isOn;
    foreach (var l in labels) 
    {
      l.color = ChangeColor(toggle.isOn);
    }
    foreach (var s in sliders)
    {
      s.interactable = toggle.isOn;
    }
  }

  Color ChangeColor(bool b)
  {
    if (b) return new Color(0.972549f, 0.7333333f, 0f);
    return Color.gray;
  }
}
