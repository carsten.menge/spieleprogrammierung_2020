﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Default Projectile class.
/// </summary>
public class DefaultProjectile : Projectile
{
  public float dmg;
  public override void OnHitEntity(GameEntity target)
  {
    Destroy(this.gameObject);
  }

  public override void OnHitEnvironment(Collider target)
  {
    Destroy(this.gameObject);
  }

  public override void OnHitIHealth(IHealth target)
  {
    target.ApplyDamage(dmg);
    Destroy(this.gameObject);
  }
}
