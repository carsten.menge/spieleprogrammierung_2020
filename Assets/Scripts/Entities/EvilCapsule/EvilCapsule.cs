﻿using Assets.Scripts;
using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Evil Capsule Enemy
/// Stays dormant until the player enters its AggroTrigger. Then hunts the player down until he leaves the Trgger area.
/// Returns to spawn after becoming dormant again. Deals contact damage.
/// </summary>
public class EvilCapsule : Enemy, IHealth, IGameStateListener
{
  public NavMeshAgent navAgent;
  public GameObject mesh;
  public int dmgPerSecond = 10;
  public bool active = false;
  public enum State { Dormant, Hunting, Afraid }
  public State CurrentState;

  public float Health { get; set; }

  private Renderer meshRenderer;

  public override void InitializeEnemyBehaviours()
  {
    meshRenderer = mesh.GetComponent<Renderer>();
    Health = 100;
    CurrentState = State.Dormant;
  }

  void Update()
  {
    switch (CurrentState)
    {
      case State.Dormant:
        navAgent.SetDestination(SpawnPoint.transform.position);
        break;
      case State.Hunting:
        GameObject target = GameManager.playerMarble.gameObject;
        navAgent.SetDestination(target.transform.position);
        break;
      case State.Afraid:
        break;
      default:
        break;
    }
    if (!IsAlive())
    {
      StartCoroutine(DeathAnimation());
    }
  }

  void OnTriggerStay(Collider other)
  {
    float distance;
    distance = Vector3.Distance(other.ClosestPointOnBounds(transform.position), transform.position);

    IHealth damagableObject;
    if (distance < .8f && other.gameObject.TryGetComponent<IHealth>(out damagableObject))
    {
      damagableObject.ApplyDamage(dmgPerSecond * Time.deltaTime);
    }
  }

  public bool IsAlive()
  {
    return Health > 0;
  }

  public void ApplyDamage(float dmgToApply)
  {
    Health -= dmgToApply;
    StartCoroutine(ColorChangeOnDamage());
  }
  /// <summary>
  /// Heal this entity.
  /// </summary>
  /// <param name="amountToHeal">Amount to heal as Float</param>
  public void Heal(float amountToHeal)
  {
    Health += amountToHeal;
  }

  public void OnUpdateGameState(GameState gameState)
  {
    if (active && gameState.State != GameState.States.GameRunning)
    {
      navAgent.isStopped = true;
    }
  }
  /// <summary>
  /// Simple animation for taking damage.
  /// </summary>
  private IEnumerator ColorChangeOnDamage()
  {
    WaitForSeconds delay = new WaitForSeconds(.2f);
    meshRenderer.material.color = Color.red;
    yield return delay;
    meshRenderer.material.color = new Color32(245, 163, 0, 255);
  }
  /// <summary>
  /// Simple particle effect for death.
  /// </summary>
  /// <returns></returns>
  private IEnumerator DeathAnimation()
  {
    ParticleSystem p = GetComponent<ParticleSystem>();
    WaitForSeconds delay = new WaitForSeconds(p.main.duration);
    p.Play();
    GetComponentInChildren<MeshRenderer>().enabled = false;
    dmgPerSecond = 0;
    yield return delay;
    Destroy(gameObject);
  }
}
