﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Rotating entity to push the player around.
/// </summary>
public class Spinnythingy : GameEntity
{
  public GameObject axle;
  public float rotationSpeed = 32f;
  public bool invertSpinning;

  public override void InitializeBehaviour()
  {
    transform.position = SpawnPoint.transform.position;
    if (invertSpinning) rotationSpeed = -rotationSpeed;
  }

  void FixedUpdate()
  {
    axle.transform.Rotate(transform.up, Time.fixedDeltaTime * rotationSpeed);
  }
  /// <summary>
  /// Invert the direction of the spinning.
  /// </summary>
  public void InvertSpinning()
  {
    transform.position = SpawnPoint.transform.position;
    rotationSpeed = -rotationSpeed;
  }
}
