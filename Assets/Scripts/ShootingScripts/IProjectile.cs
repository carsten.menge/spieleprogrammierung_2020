﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Interface for projectiles.
/// </summary>
public interface IProjectile
{
  /// <summary>
  /// Called when the projectile hits an entity implementing IHealth.
  /// </summary>
  /// <param name="target">The hit target.</param>
  void OnHitIHealth(IHealth target);
  /// <summary>
  /// Called when the projectile hits any GameEntity.
  /// </summary>
  /// <param name="target">The hit entity.</param>
  void OnHitEntity(GameEntity target);
  /// <summary>
  /// Called when the prijectile hits any other collider.
  /// </summary>
  /// <param name="target">The hit collider.</param>
  void OnHitEnvironment(Collider target);
}
