﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Trigger for the goal.
/// </summary>
public class GoalTrigger : MonoBehaviour
{
  void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.name == "PlayerMarble")
    {
      other.BroadcastMessage("EnteredGoalTrigger");
    } 
  }    
}
