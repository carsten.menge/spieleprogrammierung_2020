﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Viewcone Trigger for the turret
/// </summary>
public class TurretViewCone : MonoBehaviour
{
  public Turret parent;
  void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.name == "PlayerMarble")
    {
      parent.UpdateState(Turret.Mode.Attacking);
      parent.TargetGameObject = other.gameObject;
    }
  }

  void OnTriggerExit(Collider other)
  {
    if (other.gameObject.name == "PlayerMarble") parent.UpdateState(Turret.Mode.Searching);
  }
}
