﻿using Assets.Scripts.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Simple Button that other objects can reference to activate something
/// </summary>
public class BigRedButton : GameButton
{
  public GameObject button;
  public bool oneTimeActivation;
  public bool isActivated;
  public bool onlyPlayerCanActivate = true;

  private List<IGameButtonListener> listeners = new List<IGameButtonListener>();
  private Renderer btnRenderer;
  private bool oneTimeActivationDone = false;

  void Start()
  {
    btnRenderer = button.GetComponent<Renderer>();
    ChangeColor();
  }

  void OnTriggerEnter(Collider other)
  {
    if (onlyPlayerCanActivate && other.gameObject.name == "PlayerMarble")
    {
      Activate();
    } else if (!onlyPlayerCanActivate)
    {
      Activate();
    }
  }

  void ChangeColor()
  {
   if (isActivated)
    {
      btnRenderer.material.color = Color.green;
    } else
    {
      btnRenderer.material.color = Color.red;
    }
  }

  public override void Subscribe(IGameButtonListener listener)
  {
    listeners.Add(listener);
  }

  public override void Activate()
  {
    if (oneTimeActivation)
    {
      if (!oneTimeActivationDone)
      {
        oneTimeActivationDone = true;
        listeners.ForEach(l => l.OnButtonActivate(null));
        isActivated = !isActivated;
        ChangeColor();
      }
    }
    else
    {
      isActivated = !isActivated;
      listeners.ForEach(l => l.OnButtonActivate(null));
      ChangeColor();
    }
  }
}
