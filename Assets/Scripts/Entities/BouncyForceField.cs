﻿using System.Collections;
using UnityEngine;
/// <summary>
/// A Trigger that rebounces any other collider when it tries to enter it.
/// </summary>
public class BouncyForceField : MonoBehaviour
{
  private Vector3 deflectionNormal;
  void Start()
  {
    deflectionNormal = transform.TransformDirection(Vector3.right);
  }

  void OnTriggerEnter(Collider other)
  {
    Rigidbody r;
    ParticleSystem p = GetComponentInChildren<ParticleSystem>();
    Light l = GetComponentInChildren<Light>();
    if (other.TryGetComponent<Rigidbody>(out r))
    {
      r.velocity = Vector3.Reflect(r.velocity, deflectionNormal);
      p.gameObject.transform.position = r.transform.position;
      
      StartCoroutine(Bzzt(p, l));
    }
  }
  /// <summary>
  /// Plays a particle and light effect on bouncing.
  /// </summary>
  /// <param name="p">The Particle System to be used.</param>
  /// <param name="l">The light effect to use.</param>
  /// <returns></returns>
  IEnumerator Bzzt(ParticleSystem p, Light l)
  {
    p.Play();
    l.enabled = true;
    yield return new WaitForSeconds(p.main.duration);
    l.enabled = false;
  }
}
