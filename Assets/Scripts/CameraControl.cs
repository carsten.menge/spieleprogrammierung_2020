﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.WebCam;
/// <summary>
/// Camera control class.
/// </summary>
public class CameraControl : MonoBehaviour
{
  public GameManager gameManager;
  public Vector3 cameraOffset = new Vector3(0, 10, -10);
  public float minFOV = 30f;
  public float maxFOV = 75f;
  public float zoomSensitivity = 15f;
  public float lookAroundSensitivity = 3f;

  void Update()
  {
    // if the relative control scheme is active, make the camera rotate around the player marble...
    if (!gameManager.absoluteControlScheme)
    {
      Quaternion camTurn;
      if (Input.GetAxis("QE Axis") != 0)
      {
        camTurn = Quaternion.AngleAxis(Input.GetAxis("QE Axis") * lookAroundSensitivity, Vector3.up);
      }
      else
      {
        camTurn = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * lookAroundSensitivity, Vector3.up);
      }
      cameraOffset = camTurn * cameraOffset;
    } else // else, just put it behind the marble.
    {
      cameraOffset = new Vector3(0, 10, -10);
    }
    transform.position = gameManager.playerMarble.transform.position + cameraOffset;
    transform.LookAt(gameManager.playerMarble.transform.position);

    // simple zoom
    float fov = Camera.main.fieldOfView;
    fov -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
    fov = Mathf.Clamp(fov, minFOV, maxFOV);
    Camera.main.fieldOfView = fov;
  }    
}
