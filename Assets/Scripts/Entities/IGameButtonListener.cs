﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Interface to be implemented by any object that waits for a button to activate.
/// </summary>
public interface IGameButtonListener
{
  /// <summary>
  /// Executed on activation of button.
  /// </summary>
  /// <param name="args"></param>
  void OnButtonActivate(Object args);
}

