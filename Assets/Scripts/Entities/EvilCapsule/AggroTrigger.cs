﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Entities
{
  /// <summary>
  /// Trigger to aggro the EvilCapsule. When the player enters, the Capsule goes into the hunting state.
  /// </summary>
  public class AggroTrigger : MonoBehaviour
  {
    public EvilCapsule parent;
    void OnTriggerEnter(Collider other)
    {
      if (other.gameObject.name == "PlayerMarble") parent.CurrentState = EvilCapsule.State.Hunting;
    }

    void OnTriggerExit(Collider other)
    {
      if (other.gameObject.name == "PlayerMarble") parent.CurrentState = EvilCapsule.State.Dormant;
    }
  }
}
